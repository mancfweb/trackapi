import mongoose from 'mongoose';

const Schema = mongoose.Schema; 

const subscriptionModel = new Schema({
    user_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    company_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Company'
    },
    plan_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Plan'
    },
    duration: { type: Number },
    expire_in: { type: Date },
    status: { type: Boolean }
},
{
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" } 
});

export default mongoose.model('Subscription', subscriptionModel)