import mongoose from 'mongoose';

const Schema = mongoose.Schema; 

const punchModel = new Schema({
    user_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    city: { type: String },
    state: { type: String }, 
    country: { type: String },
    location: { type: String },
    status: { type: boolean}
},
{
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" } 
});

export default mongoose.model('Punch', punchModel)