import mongoose from 'mongoose';

const Schema = mongoose.Schema; 

const companyModel = new Schema({
	name: { type: String, required: true },
    email: { type: String, required: true, unique: true},
    phone: { type: String },
    street: { type: String },
    city: { type: String },
    state: { type: String },
    logo: { type: String },
    plan_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Plan'
    },
    owner_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    },
    active: { type: Boolean },
    expire_date: { type: Date }
},
{
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" } 
});

export default mongoose.model('Company', companyModel)