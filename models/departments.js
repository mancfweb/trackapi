import mongoose from 'mongoose';

const Schema = mongoose.Schema; 

const departmentModel = new Schema({
	name: { type: String, required: true },
    code: { type: String, required: true, unique: true},
    punch_rounding_id: { type: Number },
    punch_for_break: { type: Number },
    break_add_up_to: { type: Number },
    paid_break_id: { type: Number },
    paid_break_duration: { type: Number },
    punch_for_lunch: { type: Number },
    lunch_add_up_to: { type: Number },
    lunch_lockout: { type: Number },
    lockout_duration: { type: Number },
    lunch_deduction_id: { type: Number },
    deduction_duration_id: { type: Number },
    deduct_time_from_timecard: { type: Number },
    deduction_recurring: { type: Number },
    overtime_id: { type: Number },
    daily_overtime1: { type: Number },
    daily_overtime2: { type: Number },
    seventh_day_overtime: { type: Number },
    saturday_overtime: { type: Number },
    sunday_overtime: { type: Number },
    which_overtime: { type: Number },
    day_change_id: { type: Number },
    max_shift: { type: Number },
    min_shift: { type: Number },
    new_shift_starts: { type: Number },
    night_shift: { type: Number },
    active: { type: Boolean }
},
{
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" } 
});

export default mongoose.model('Department', departmentModel)