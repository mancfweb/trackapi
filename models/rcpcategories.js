import mongoose from 'mongoose';

const Schema = mongoose.Schema; 

const rcpcategoriesModel = new Schema({
	name: { type: String, required: true },
    description: { type: String, required: true},
    status: { type: Boolean }
},
{
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" } 
});

export default mongoose.model('Rcpcategory', rcpcategoriesModel)