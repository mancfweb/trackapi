import mongoose from 'mongoose';

const Schema = mongoose.Schema; 

const planModel = new Schema({
	name: { type: String, required: true },
    description: { type: String },
    price: { type: mongoose.Schema.Types.Decimal128 },
    active: { type: Boolean }
},
{
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" } 
});

export default mongoose.model('Plan', planModel)