import mongoose from 'mongoose';

const Schema = mongoose.Schema; 

const transactionModel = new Schema({
    subscription_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Subscription'
    },
    price: { type: mongoose.Schema.Types.Decimal128 },
    paypal_token: { type: String },
    status: { type: Number }
},
{
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" } 
});

export default mongoose.model('Transaction', transactionModel)