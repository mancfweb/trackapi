import mongoose from 'mongoose';

const Schema = mongoose.Schema; 

const receiptModel = new Schema({
	name: { type: String, required: true },
    description: { type: String, required: true},
    file: { type: String },
    category_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Rcpcategory'
    },
    user_id: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Users'
    }
},
{
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" } 
});

export default mongoose.model('Note', noteModel)