import express from 'express';
import Company from '../models/companies';
const companyRouter = express.Router();

companyRouter.route('/')
    .get((req, res) => {
        Company.find({}, (err, companies) => {
            res.json(companies)
        })  
    })
    .post((req, res) => {
        let company = new Company(req.body);
        company.save();
        res.status(201).send(company) 
    })

// Middleware 
companyRouter.use('/:companyId', (req, res, next)=>{
    Company.findById( req.params.companyId, (err,company)=>{
        if(err)
            res.status(500).send(err)
        else {
            req.company = company;
            next()
        }
    })

})
companyRouter.route('/:companyId')
    .get((req, res) => {
        res.json(req.company)
    }) // end get companies/:companyId 
    .put((req,res) => {
        req.company.name = req.body.name;
        req.company.email = req.body.email;
        req.company.phone = req.body.phone;
        req.company.street = req.body.street;
        req.company.city = req.body.city;
        req.company.state = req.body.state;
        req.company.logo = req.body.logo;
        req.company.plan_id = req.body.plan_id;
        req.company.owner_id = req.body.owner_id;
        req.company.active = req.body.active;
        req.company.save()
        res.json(req.company)
    })
    .patch((req,res)=>{
        if(req.body._id){
            delete req.body._id;
        }
        for( let p in req.body ){
            req.company[p] = req.body[p]
        }
        req.company.save()
        res.json(req.company)
    })//patch
    .delete((req,res)=>{
        req.company.remove(err => {
            if(err){
                res.status(500).send(err)
            }
            else{
                res.status(204).send('removed')
            }
        })
    })//delete
	 
export default companyRouter;