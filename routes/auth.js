import express from 'express';
import jwt from 'jsonwebtoken';
import Users from '../models/users';

const authRouter = express.Router();

authRouter.post('/login', (req, res, next) => {
    Users.findOne({
        username: req.body.username
    }, (err, user) => {
        if (err) throw err;

        if (!user) {
        res.status(401).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {
        // check if password matches
        user.comparePassword(req.body.password,  (err, isMatch) => {
            if (isMatch && !err) {
                // if user is found and password is right create a token
                var token = jwt.sign(user.toJSON(), 'mancftrackmyguysapp',{ expiresIn: '120m' });
                // return the information including token as JSON
                res.json({success: true, token: 'JWT ' + token, userid: user._id, username: user.name, userlogin: req.body.username });
            } else {
                res.status(401).send({success: false, msg: 'Authentication failed. Wrong password.'});
            }
        });
        }
    });
})
 
authRouter.get('/logout', function(req,res){
    res.status(200).send({ auth: false, token: null});
    req.session = null;
})

export default authRouter;