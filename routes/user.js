import express from 'express';
import Users from '../models/users';
import isAutenthicate from '../utils/utils';

const userRouter = express.Router();

userRouter.route('/', isAutenthicate)
.post((req, res, next) => {
    let user = new Users(req.body);
      
    if(!user.name) {
      return res.status(422).json({
        errors: {
          name: 'is required',
        },
      });
    }
      
    if(!user.username) {
      return res.status(422).json({
        errors: {
          username: 'is required',
        },
      });
    }
  
    if(!user.password) {
      return res.status(422).json({
        errors: {
          password: 'is required',
        },
      });
    }
  
    return user.save()
    .then(() => res.status(201).send(user)); 
})
.get((req, res) => {
  Users.find({}, (err, users) => {
      res.json(users)
  })  
})

//GET current route (required, only authenticated users have access)
userRouter.get('/current', isAutenthicate, (req, res, next) => {
const { payload: { id } } = req;

return Users.findById(id)
    .then((user) => {
    if(!user) {
        return res.sendStatus(400);
    }

    return res.json({ user: user });
    });
});


// Middleware 
userRouter.use('/:userId', isAutenthicate, (req, res, next)=>{
  Users.findById( req.params.userId, (err,user)=>{
      if(err)
          res.status(500).send(err)
      else {
          req.user = user;
          next()
      }
  })

})
userRouter.route('/:userId')
  .get((req, res) => {
      res.json(req.user)
  }) // end get users/:userId 
  .put((req,res) => {
        req.user.name = req.body.name;
        req.user.username = req.body.username;
        req.user.password = req.body.password;
        req.user.email = req.body.name;
        req.user.street = req.body.name;
        req.user.city = req.body.name;
        req.user.state = req.body.name;
        req.user.language = req.body.name;
        req.user.phone = req.body.name;
        req.user.hourprice = req.body.name;
        req.user.departament_id = req.body.name;
        req.user.company_id = req.body.name;
        req.user.role = req.body.name;
        req.user.status = req.body.name;
        req.user.save()
        res.json(req.user)
  })
  .patch((req,res)=>{
      if(req.body._id){
          delete req.body._id;
      }
      for( let p in req.body ){
          req.user[p] = req.body[p]
      }
      req.user.save()
      res.json(req.user)
  })//patch
  .delete((req,res)=>{
      req.user.remove(err => {
          if(err){
              res.status(500).send(err)
          }
          else{
              res.status(204).send('removed')
          }
      })
  })//delete

export default userRouter;