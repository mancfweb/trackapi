import express from 'express';
import cookieParser from 'cookie-parser';
import logger from 'morgan';
import mongoose from 'mongoose';
import helmet from 'helmet';
import cors from 'cors';
import bodyParser from 'body-parser';
import authRouter from './routes/auth';
import userRouter from './routes/user';

const app = express();
const port = 5658;
const WEBAPP_URI = "http://localhost:4200";
const DB = "trackmyguys";

// Connecting to the database
const db = mongoose.connect(`mongodb://localhost:27017/${DB}`);

// setting body parser middleware 
const corsOptions = {
	origin: [WEBAPP_URI],
	exposedHeaders: ['msg', 'error-msg'],
	credentials: true
}
app.use(cors(corsOptions));
app.use(logger('dev'));
app.use(helmet());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// API routes
app.use('/', authRouter);
app.use('/users', userRouter);

// Running the server
app.listen(port, () => {
	console.log(`http://localhost:${port}`)
})